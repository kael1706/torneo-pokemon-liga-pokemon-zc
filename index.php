<?php

require 'config.php';

function say($what){
    print "<div class='r'>".$what."</div>";
}
function say2($what){
    print "<div class='r'><br>".$what."</br></div>";
}

/*
TIPOS
$tipo = new Tipo(nombre,fortalezas[],debilidades[]);
*/
$TAgua = TipoFactory::getTipo("agua", ["fuego","roca"],["planta","hielo"]);
$THielo = TipoFactory::getTipo("hielo",["agua"],["fuego"]) ;
$TRoca = TipoFactory::getTipo("roca",["fuego"],["agua"]);
$TPlanta = TipoFactory::getTipo("planta",["agua"],["fuego"]);
$TNeutro = TipoFactory::getTipo("neutro",null,null);
$TFuego = TipoFactory::getTipo("fuego", ["hielo","planta"], ["agua","roca"]);

/*
ATAQUES
$ataque = new Ataque(ataque,tipo,daño,pp);
*/
$Allamarada = AtaqueFactory::getAtaque("llamarada", $TFuego, 20, 10);
$Aburbuja = AtaqueFactory::getAtaque("burbuja", $TAgua, 18, 15);
$Ahiperpropulsion = AtaqueFactory::getAtaque("hiperpropulsion", $TAgua, 25, 5);
$Agarrahelada = AtaqueFactory::getAtaque("garra helada", $THielo, 10, 30);
$Acongelacion = AtaqueFactory::getAtaque("congelacion", $THielo, 30, 5);
$Aderrumbe = AtaqueFactory::getAtaque("derrumbe", $TRoca, 15, 20);
$Arocagiratoria = AtaqueFactory::getAtaque("roca giratoria", $TRoca, 25, 10);
$Alatigo = AtaqueFactory::getAtaque("latigo", $TPlanta, 15, 20);
$Ahojacortante = AtaqueFactory::getAtaque("hoja cortante", $TPlanta,30 , 5);
$AbolaFuego = AtaqueFactory::getAtaque("bola Fuego", $TFuego, 15, 20);

//ATAQUES NEUTROS
$Abombamortaldestructora = AtaqueFactory::getAtaque("bomba mortal destructora", $TNeutro,1,1);
$Aplacaje = AtaqueFactory::getAtaque("placaje",$TNeutro, 10, 20);
$Acarrera = AtaqueFactory::getAtaque("carrera", $TNeutro, 20, 10);

/*
POKEMONES
$nombre = new Pokemon(nombre,sexo,tipo,nivel);
*/
$Pcharmander = PokemonFactory::getPokemon("charmander", "M", $TFuego, [$AbolaFuego,$Allamarada,$Abombamortaldestructora,$Aplacaje], 100, false, 60);
$Pbulbasaur = PokemonFactory::getPokemon("bulbasaur", "F", $TPlanta, [$Alatigo,$Ahojacortante,$Acarrera, $Aplacaje],100, false, 60);
$Psquirtle = PokemonFactory::getPokemon("squirtle", "M", $TAgua, [$Aburbuja,$Ahiperpropulsion,$Acongelacion, $Aplacaje],100, false, 60);
$Pcaterpie = PokemonFactory::getPokemon("caterpie", "M", $TPlanta, [$Alatigo,$Ahojacortante,$Aburbuja, $Aplacaje],100, false, 60);
$Ppidgey = PokemonFactory::getPokemon("pidgey", "M", $TNeutro, [$Acarrera,$Ahojacortante,$Abombamortaldestructora, $Aplacaje],100, false, 60);
$Pvulpix = PokemonFactory::getPokemon("vulpix", "F", $TFuego, [$Acarrera,$AbolaFuego,$Abombamortaldestructora, $Aplacaje],100, false, 60);
$Ptentacool = PokemonFactory::getPokemon("tentacool", "F", $TAgua, [$Ahiperpropulsion,$Arocagiratoria,$Abombamortaldestructora, $Aplacaje],100, false, 60);
$Pgolem = PokemonFactory::getPokemon("golem", "F", $TRoca, [$Aderrumbe,$Arocagiratoria,$Abombamortaldestructora, $Acarrera],100, false, 60);
$Particuno = PokemonFactory::getPokemon("articuno", "M", $THielo, [$Agarrahelada,$Acongelacion,$Abombamortaldestructora, $Acarrera],100, true, 60);
$Psnorlax = PokemonFactory::getPokemon("snorlax", "M", $TNeutro, [$Acongelacion,$Ahojacortante,$Abombamortaldestructora, $Aplacaje],100, false, 60);
$Prattata = PokemonFactory::getPokemon("rattata", "M", $TNeutro, [$Acarrera,$Alatigo,$Acongelacion, $Aplacaje],100, false, 60);
$Praticate = PokemonFactory::getPokemon("raticate", "M", $TNeutro, [$Acarrera,$Alatigo,$Acongelacion, $Aplacaje],10, false, 60);

/*
TRAINERS
*/
$Pkgeral=[$Pcharmander,$Pvulpix,$Pgolem];
$Pknikolaz=[$Psnorlax,$Ptentacool,$Psquirtle];
$Pkjhonatan=[$Pcharmander,$Pbulbasaur,$Particuno]; //descalificado legendario
$Pkcarlos=[$Pcaterpie,$Pvulpix,$Pgolem];
$Pkdaniel=[$Psnorlax,$Psquirtle,$Ppidgey];
$Pkdaniela=[$Pbulbasaur,$Psquirtle,$Pgolem];
$Pkfernando=[$Prattata,$Pcaterpie,$Psquirtle];
$Pkfernanda=[$Ptentacool,$Pvulpix,$Pcaterpie];
$Pkluis=[$Ptentacool,$Ptentacool,$Prattata];
$Pkluisa=[$Pvulpix,$Psnorlax,$Pcaterpie];
$Pkcamilo=[$Ppidgey,$Pcharmander,$Psquirtle];
$Pkcamila=[$Prattata,$Prattata,$Psnorlax];
$Pkandrea=[$Praticate,$Pvulpix,$Psnorlax];
$Pkandres=[$Pcharmander,$Psquirtle,$Ppidgey];
$Pkjulian=[$Psquirtle,$Ptentacool,$Pgolem];
$Pkash=[$Pcharmander,$Pbulbasaur,$Psquirtle];


$Trash = TrainerFactory::getTrainerAmistoso("ash", 10, 18, "Paleta",$Pkash);
$Trgeral = TrainerFactory::getTrainerAmistoso("geral", 20, 15, "Cali",$Pkgeral);
$Trnikolaz = TrainerFactory::getTrainerAmistoso("nikolaz", 15, 22, "Pasto",$Pknikolaz);
$Trjhonatan = TrainerFactory::getTrainerAmistoso("jhonatan", 15, 22, "Medellin",$Pkjhonatan);
$Trcarlos = TrainerFactory::getTrainerAmistoso("carlos", 12, 22, "Ibague",$Pkcarlos);
$Trdaniel = TrainerFactory::getTrainerAmistoso("Daniel", 12, 22, "Cali",$Pkdaniel);
$Trdaniela = TrainerFactory::getTrainerAmistoso("Daniela", 15, 10, "Quito",$Pkdaniela);
$Trfernando = TrainerFactory::getTrainerAmistoso("Fernando", 30, 40, "Pasto",$Pkfernando);
$Trfernanda = TrainerFactory::getTrainerAmistoso("Fernanda", 30, 30, "Medellin",$Pkfernanda);
$Trluis = TrainerFactory::getTrainerAmistoso("Luis", 31, 35, "Medellin",$Pkluis);
$Trluisa = TrainerFactory::getTrainerAmistoso("Luisa", 11, 25, "Cartagena",$Pkluisa);
$Trcamilo = TrainerFactory::getTrainerAmistoso("Camilo", 11, 23, "Cartagena",$Pkcamilo);
$Trcamila = TrainerFactory::getTrainerAmistoso("Camila", 12, 23, "Pasto",$Pkcamila);
$Trandrea = TrainerFactory::getTrainerAmistoso("Andrea", 9, 23, "Quito",$Pkandrea);
$Trandres = TrainerFactory::getTrainerAmistoso("Andres", 5, 13, "Cali",$Pkandres);
$Trjulian = TrainerFactory::getTrainerAmistoso("Julian", 3, 10, "Ibague",$Pkjulian);

$trainers = [$Trash,$Trgeral,$Trnikolaz,$Trjhonatan,$Trcarlos,$Trdaniel,$Trdaniela,$Trfernando,$Trfernanda,$Trluis];
/*
Batalla
// */
$batalla1 = BatallaFactory::getBatalla($Trash, $Trgeral);


/*
PRINTS
// */
say2("TIPO----");
say2("Nombre:");
say($TFuego->getNombre());
say2("Fortaleza:");
for ($i = 0; $i < count($TFuego->getFortalezas()); $i++) {               
    say($TFuego->getFortalezas()[$i]);        
}
say2("Debilidades:");
for ($i = 0; $i < count($TFuego->getDebilidades()); $i++) {               
    say($TFuego->getDebilidades()[$i]);        
}


say2("POKEMON--------");
say2("Nombre:");
say($Pcharmander->getNombre());
say2("Genero:");
say($Pcharmander->getGenero());
say2("Tipo:");
say($Pcharmander->getTipo()->getNombre());
say2("Ataques:");
for ($i = 0; $i < count($Pcharmander->getAtaques()); $i++) {               
    say($Pcharmander->getAtaques()[$i]->getNombre());        
}
say2("Nivel:");
say($Pcharmander->getNivel());
say2("Hp:");
say($Pcharmander->getHp());
say2("Resumen:");
say($Pcharmander->MyInfo());

 
say2("TRAINER AMISITOSO--------");
say2("Nombre:");
say($Trash->getNombre());
say2("Edad:");
say($Trash->getEdad());
say2("Medallas:");
say($Trash->getMedallas());
say2("Pokemons:");
for ($i = 0; $i < count($Trash->getPokemons()); $i++) {
    say($Trash->getPokemons()[$i]->getNombre());
}   
say2("Espiritu:");
say($Trash->getEspiritu());


$tournament = new Tournament($trainers, "zuliTournament");
$tournament->TrainerRegistro();
$tournament->TournamentComposicion();
$tournament->Emparejar();
$tournament->getBatallas()[0]->TurnosBatalla();
/*
say2("BATALLA--------");
say2("Trainer1:");
say($batalla1->getTrainer1()->getNombre());
say2("Trainer2:");
say($batalla1->getTrainer2()->getNombre());
$batalla1->TurnosBatalla();*/