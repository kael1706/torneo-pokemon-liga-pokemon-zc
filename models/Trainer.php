<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trainer
 *
 * @author pabhoz
 */
class Trainer {
    private $nombre, $medallas, $edad, $pueblo;
    private $pokemons=[];
    
    function __construct($nombre, $medallas, $edad, $pueblo,$pokemons) {
        $this->nombre = $nombre;
        $this->medallas = $medallas;
        $this->edad = $edad;
        $this->pueblo = $pueblo;
        $this->pokemons = $pokemons;
        
    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getMedallas() {
        return $this->medallas;
    }

    function getEdad() {
        return $this->edad;
    }

    function getPueblo() {
        return $this->pueblo;
    }

    function getPokemons() {
        return $this->pokemons;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setMedallas($medallas) {
        $this->medallas = $medallas;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setPueblo($pueblo) {
        $this->pueblo = $pueblo;
    }

    function setPokemons($pokemons) {
        $this->pokemons = $pokemons;
    }

    function getEspiritu() {
        return $this->espiritu;
    }
    
    public function lanzarPokemon($pkpos){
        
        echo $this->getNombre()." lanza a ".$this->getPokemons()[$pkpos]->getNombre()."<br>";
    }
}
