<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pokemon
 *
 * @author pabhoz
 */
class Pokemon {
    private $nombre;
    private $genero;
    private $tipo;
    private $ataques = [];
    private $nivel;
    private $legendario;
    private $hp;
    
    function __construct($nombre, $genero, Tipo $tipo, $ataques, $nivel, $legendario,$hp) {
        $this->nombre = $nombre;
        $this->genero = $genero;
        $this->tipo = $tipo;
        $this->ataques = $ataques;
        $this->nivel = $nivel;
        $this->legendario = $legendario;
        $this->hp = $hp;
    }
    
    
    function getNombre() {
        return $this->nombre;
    }

    function getGenero() {
        return $this->genero;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getAtaques() {
        return $this->ataques;
    }

    function getNivel() {
        return $this->nivel;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setAtaques($ataques) {
        $this->ataques = $ataques;
    }

    function setNivel($nivel) {
        $this->nivel = $nivel;
    }
    
    function getLegendario() {
        return $this->legendario;
    }

    function setLegendario($legendario) {
        $this->legendario = $legendario;
    }
    
    function getHp() {
        return $this->hp;
    }

    function setHp($hp) {
        $this->hp = $hp;
    }

        
    public function MyInfo(){
        
       for ($i = 0; $i < count($this->getAtaques()); $i++) {
            
           if ($i!=0){
                $str_Ataques= $str_Ataques.", ".$this->getAtaques()[$i]->getNombre();
            } else {
                $str_Ataques= $this->getAtaques()[$i]->getNombre();
            }
            
        }
        
        return "(".$this->getNombre().", ".$this->getGenero().", ".$this->getTipo()->getNombre().
                ", [".$str_Ataques."], ".$this->getNivel().", ".$this->getHp().")";
            
    }
    
    public function atacar($victima,$atacante){
        
        $this->getAtaques()[$atacante]->setPp($this->getAtaques()[$atacante]->getPp()-1);
        $preHp=$victima->getHp()-($this->getAtaques()[$atacante]->getDanio()+ $this->getAtaques()[$atacante]->getTipo()->bonus($this->getAtaques()[$atacante]->getTipo(),$victima->getTipo()));
        if($preHp<0){
           $preHp=0; 
        }
        $victima->setHp($preHp);
        
        echo $this->getNombre()." usa ".$this->getAtaques()[$atacante]->getNombre()." contra ".
                $victima->getNombre()." y le causa ".$this->getAtaques()[$atacante]->getDanio()."Daño<br>";
        
        echo $victima->getNombre()." queda con ".$victima->getHp()."HP<br>";
    }
}
