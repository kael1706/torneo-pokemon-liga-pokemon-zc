<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ataque
 *
 * @author carlosdaniel
 */
class Ataque {
    private $nombre;
    private $tipo;
    private $danio;
    private $pp;
    
    function __construct($nombre, $tipo, $danio, $pp) {
        $this->nombre = $nombre;
        $this->tipo = $tipo;
        $this->danio = $danio;
        $this->pp = $pp;
    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getDanio() {
        return $this->danio;
    }

    function getPp() {
        return $this->pp;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setDanio($danio) {
        $this->danio = $danio;
    }

    function setPp($pp) {
        $this->pp = $pp;
    }


}
