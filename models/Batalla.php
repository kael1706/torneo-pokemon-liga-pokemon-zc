<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Batalla
 *
 * @author carlosdaniel
 */
class Batalla {
    
    private $trainer1;
    private $trainer2;
    private $ganador;
    public $p;
    private $turno=0;
    public $pkpos1=0;
    public $pkpos2=0;
    public $contador=0;
    public $contador2=0;
    function __construct( $trainer1,  $trainer2) {
        $this->trainer1 = $trainer1;
        $this->trainer2 = $trainer2;
    }

    function getTrainer1() {
        return $this->trainer1;
    }

    function getTrainer2() {
        return $this->trainer2;
    }

    function getGanador() {
        return $this->ganador;
    }

    function setTrainer1($trainer1) {
        $this->trainer1 = $trainer1;
    }

    function setTrainer2($trainer2) {
        $this->trainer2 = $trainer2;
    }

    function setGanador($ganador) {
        $this->ganador = $ganador;
    }
    
    function getTurno() {
        return $this->turno;
    }

    function setTurno($turno) {
        $this->turno = $turno;
    }

    public function  TurnosBatalla(){
        
        if($this->getTurno()%2==0){
            if(!($this->getTrainer1()->getPokemons()[2]->getHp()<=0)){
                if ($this->getTrainer1()->getPokemons()[$this->pkpos1]->getHp()<=0 || $this->getTurno()==0){
                    $this->pkpos1=$this->contador;
                    $this->getTrainer1()->lanzarPokemon($this->pkpos1);

                    $this->contador=$this->contador+1;

                       

                }else{
                    $this->getTrainer1()->getPokemons()[$this->pkpos1]->atacar($this->getTrainer2()->getPokemons()[$this->pkpos2],$this->pkpos1);
                }
            }else{
                $this->p=true;
                echo "El ganador es ".$this->getTrainer2()->getNombre();
            }
            if($this->p!=true){
                $this->setTurno($this->getTurno()+1);
                $this->TurnosBatalla();
            }
        }else{
            if(!($this->getTrainer2()->getPokemons()[2]->getHp()<=0)){
                if ($this->getTrainer2()->getPokemons()[$this->pkpos2]->getHp()<=0 || $this->getTurno()==1){
                    $this->pkpos2=$this->contador2;
                    $this->getTrainer2()->lanzarPokemon($this->pkpos2);

                   $this->contador2=$this->contador2+1;

                       

                }else{
                    $this->getTrainer2()->getPokemons()[$this->pkpos2]->atacar($this->getTrainer1()->getPokemons()[$this->pkpos1],$this->pkpos2);
                }
            }else{
                $this->p=true;
                echo "El ganador es ".$this->getTrainer1()->getNombre();
            }
            if($this->p!=true){
               $this->setTurno($this->getTurno()+1);
               $this->TurnosBatalla(); 
            }
            
            
        } 
         
    }

}
