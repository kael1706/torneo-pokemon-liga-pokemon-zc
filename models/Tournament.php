<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tournament
 *
 * @author pabhoz
 */
class Tournament{
    //put your code here
    private $trainers = [];
    private $nombre;
    private $posiciones =[];
    private $rondas;
    public $aprobados = [];
    public $aprobadosR = [];
    private $batallas=[];
    
    function __construct($trainers, $nombre) {
        $this->trainers = $trainers;
        $this->nombre = $nombre;
    }

    function getTrainers() {
        return $this->trainers;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getPosiciones() {
        return $this->posiciones;
    }

    function getRondas() {
        return $this->rondas;
    }

    function setTrainers($trainers) {
        $this->trainers = $trainers;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setPosiciones($posiciones) {
        $this->posiciones = $posiciones;
    }

    function setRondas($rondas) {
        $this->rondas = $rondas;
    }

    function getBatallas() {
        return $this->batallas;
    }

    function setBatallas($batallas) {
        $this->batallas = $batallas;
    }

        public function TrainerRegistro(){
        for ($i = 0; $i < count($this->getTrainers()); $i++) {
            if($this->getTrainers()[$i]->getMedallas() < 10 || 
                    count($this->getTrainers()[$i]->getPokemons()) != 3){
                echo $this->getTrainers()[$i]->getNombre()." no cumple con la"
                        . " cantidad de medallas o de pokemones requeridas <br>";
            }else{
                for($j = 0; $j < count($this->getTrainers()[$i]->getPokemons()); $j++){
                    $boolean = true;
                    if($this->getTrainers()[$i]->getPokemons()[$j]->getLegendario() == true){
                        echo "El pokemon ".$this->getTrainers()[$i]->getPokemons()[$j]->getNombre().""
                            . " de ".$this->getTrainers()[$i]->getNombre()." es legendario y no se puede"
                                . " inscribir <br>";
                        $boolean = false;
                    }else{
                        if($this->getTrainers()[$i]->getPokemons()[$j]->getNivel() < 100){                       
                            echo "El pokemon ".$this->getTrainers()[$i]->getPokemons()[$j]->getNombre().""
                                . " de ".$this->getTrainers()[$i]->getNombre()." no cumple con el"
                                . " nivel indicado <br>";

                            $boolean = false;
                        }
                    }
                }
                if($boolean){
                    echo $this->getTrainers()[$i]->getNombre()." ahora está inscrito"
                    . " en el torneo <br>";
                    array_push($this->aprobados, $this->getTrainers()[$i]);
                }
            }
        }
    }

    public function TournamentComposicion(){
        while((count($this->aprobados) % 8) != 0){
            for($i=0; $i < (count($this->aprobados)) ; $i++){
                echo $this->aprobados[$i]->getNombre()."<br>";
            }
            $val = rand(0, (count($this->aprobados)));
            array_splice($this->aprobados,$val,1);
            echo "------------------ <br>";
            for($i=0; $i < (count($this->aprobados)) ; $i++){
                
                echo $this->aprobados[$i]->getNombre()."<br>";
            }
        }
        $aprobadosR=$this->aprobados;
    }
    
    public function Emparejar(){
        $tamano=count($this->aprobados);
        
        for($i=0; $i < ($tamano/2) ; $i++){
         
                $val = rand(0, (count($this->aprobados))-1);
                $trainer1=$this->aprobados[$val];
                array_splice($this->aprobados,$val,1);
                $val2 = rand(0, (count($this->aprobados))-1);
                
                $trainer2=$this->aprobados[$val2];
                array_splice($this->aprobados,$val2,1);

                $batalla= new Batalla($trainer1,$trainer2);
                
                array_push($this->batallas, $batalla);
                $this->setBatallas($this->batallas);
        }
            
            echo "-------//----------- <br>";
            
            for($i=0; $i < (4) ; $i++){
                echo "# de Batallas es:"."4"."<br>";
                echo "Batalla #".$i."<br>";
                echo $this->getBatallas()[$i]->getTrainer1()->getNombre()."<br>";
                echo $this->getBatallas()[$i]->getTrainer2()->getNombre()."<br>";
            }    
        
    }
}
