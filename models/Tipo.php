<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tipo
 *
 * @author carlosdaniel
 */
class Tipo {
    private $nombre;
    private $fortalezas=[];
    private $debilidades=[];
    
    function __construct($nombre, $fortalezas, $debilidades) {
        $this->nombre = $nombre;
        $this->fortalezas = $fortalezas;
        $this->debilidades = $debilidades;

    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getFortalezas() {
        return $this->fortalezas;
    }

    function getDebilidades() {
        return $this->debilidades;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setFortalezas($fortalezas) {
        $this->fortalezas = $fortalezas;
    }

    function setDebilidades($debilidades) {
        $this->debilidades = $debilidades;
    }

    public function bonus($tipoAtaque, $tipoVictima){
        
        $valor_bonus;
        //$this->getAtaques()[0]->getTipo()
        // $tipoAtaque->getFortalezas()[0];
        for ($i = 0; $i < count($tipoAtaque->getFortalezas()); $i++) {
            
            for ($j = 0; $j < count($tipoAtaque->getDebilidades()); $j++) {
                
                if($tipoAtaque->getFortalezas()[$i] == $tipoVictima->getNombre()){
                    ($valor_bonus < 2)? $valor_bonus=2: false;
                }elseif ($tipoAtaque->getDebilidades()[$j]== $tipoVictima->getNombre()) {
                    ($valor_bonus < 1)? $valor_bonus=0: false;
                } else {
                    ($valor_bonus < 2)? $valor_bonus=1: false;
                }
            }
        }  
        
        
        return $valor_bonus;
        
    }
}
