# Taller de pruebas y Patrones de diseño

## El desafío Pokemón: La Liga Pokemón

Cada año los mejores entrenadores del mundo se unen para registrarse al torneo pokemón, el torneo es un despliegue maravilloso de habilidosos Pokemones que luchan para demostrar quién es el mejor.

### 1. El Registro al torneo

1. Cualquier entrenador con **10 medallas** podrá registrarse en el torneo.
2. Cada entrenador podrá registrar **3 Pokemones** dentro del torneo.
3. El intento de captura de los pokemones de otro entrenador en batalla será penalizado con expulsión del torneo.
4. Los entrenadores podrán registrar pokemones únicamente en el nivel más alto **100**.
5. No es posible registrar pokemones **legendarios**.
6. Si algún entrenador no cumple con las condiciones para registrarse, el sistema aleatoreamente eliminará entrenadores hasta poder hacer al menos 3 rondas de luchas antes de decidir el ganador.
7. Al Iniciar el torneo los contrincantes serán emparejados aleatoreamente.

### 2. La Ficha de Entrenador

1. **Nombre**
2. **Medallas:** Número de medallas adquiridas
3. **Edad**
4. **Pueblo**
5. **Pokemons:** Arreglo de Pokemons registrados al torneo
6. **Espiritu:** Define si el entrenador es **Amistoso** (Que ama a sus pokemons y tiene +5% de probabilidad de rendirse al verlo sufrir en batalla), **Competitivo** (Tiene -5% de probabilidad de rendirse en batalla), **Aventajado** (Que tiende a hacer trampas, incrementando su probabilidad de trampas en un 5%).

### 3. La Ficha del Pokemón

1. **Nombre**
2. **Genero**
3. **HP**: Puntos de vida
3. **Tipo**
4. **Ataques** `[4]`
5. **Nivel**

### 4. Ataques

1. **Nombre**
2. **Tipo**
3. **Daño**
4. **PP**: Cantidad de veces que puede usar dicho ataque en un duelo.

### 5. Tipos

1. **Tipo**
2. **Fortalezas** `Array<Tipo>`
3. **Debilidades** `Array<Tipo>`

### 6. Normas de lucha

1. Cada entrenador podrá atacar por turnos, los ataques serán calculados de manera eficiente por el sistema, el cual detectará cuál de los ataques del pokemón es el más efectivo en contra de su contrincante.
2. Los ataques se calculan según la siguiente regla: `$damage = $this->Ataques["mejorOpción"]->getDamage() * $typeBonus;`

	En donde `$typeBonus` puede valer entre 0 y 2. Un ataque tiene type bonus de 0 si no es efectivo en contra de la clase del adversario, de 1 si no es una fortaleza en contra de la clase del adversario y 2 si es efectivo contra la clase del adversario.
	
3. Cuando un Pokemón el `HP`de un pokemón llega a 0, el pokemón se considera debilitado y el entrenador debe usar otro pokemón. Al usar otro pokemón, el entrenador que llama al nuevo pokemón, pierde su turno.

4. Cada turno, el sistema verifica el estado del duelo; y de esta manera ser hará la elección de la acción a ejecutar por el entrenador; un entrenador puede: **atacar**, **retirarse**, **atrapar**. La probabilidad de que un entrenador ataque es del 100% siempre que su pokemón actual tenga + de 30% de sus HP.

5. Si en el turno del entrenador su pokemón tiene por debajo de 30% de HP, el entrenador tendrá una probabilidad de ataque de 60%, de rendirse en un 30% y de atrapar en un 10%. Ojo, dichos porcentajes se ven afectados por el espítiru del entrenador (sólo bajo la condición de HP <= 30%).

6. Si un entrenador se queda sin Pokemóns con que luchar, el otro entrenador gana la ronda.

### 7. Fin del torneo

1. El torneo termina cuando 1 entrenador ha ganado suficientes duelos como para no tener contrincantes.
2. Al finalizar el torneo, se muestra el gráfico de las rondas marcando el ganador de cada una de ellas.
3. Los usuarios también podran desplegar la narración de los sucesos de cada ronda, en donde se podrán encontrar:
	1. El anuncio de la ronda
	2. El anuncio de los entrenadores a enfrentarse
	3. El turno de cada entrenador
	4. La elección de pokemón de cada entrenador
	5. La acción ejecutada por el entrenador
	6. El ataque usado por cada pokemón de la manera: Pikachú usa Impact Trueno, el ataque es muy eficaz (x2), el ataque le causa 200 de daño al enemigo Squartle.
	7. Muertes, retiros, trampas, descalificaciones.
4. El usuario podrá exportar los resultados del torneo en PDF y JSON.

# Fecha de entrega: Miercoles 25 de Octubre hasta las 10AM.

### La sustentación del trabajo será llevada a cabo mediante la grabación de un video presentando su trabajo, el video y el repositorio deben ser enviados en forma de link al docente; el docente será nombrado administrador del repositorio. La pantalla inicial tendrá siempre datos precargados de prueba y estos podrán ser editados porque usarán Inputs, Selects y demás elmeentos para una fácil interacción.	 

