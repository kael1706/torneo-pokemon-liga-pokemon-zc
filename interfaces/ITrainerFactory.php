<?php


interface ITrainerFactory {
    public static function getTrainer($nombre, $medallas, $edad, $pueblo, $pokemons):Trainer;
    public static function getTrainerAmistoso($nombre, $medallas, $edad, $pueblo,$pokemons): \TrainerAmistoso;
    public static function getTrainerAventajado($nombre, $medallas, $edad, $pueblo,$pokemons): \TrainerAventajado;
    public static function getTrainerCompetitivo($nombre, $medallas, $edad, $pueblo,$pokemons): \TrainerCompetitivo;
}