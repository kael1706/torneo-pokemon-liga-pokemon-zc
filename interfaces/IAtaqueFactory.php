<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IAtaqueFactory
 *
 * @author carlosdaniel 
 */
interface IAtaqueFactory {
    
     public static function getAtaque($nombre, $tipo, $danio, $pp): Ataque;
     
}
