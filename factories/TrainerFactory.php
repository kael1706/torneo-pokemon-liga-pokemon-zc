<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TrainerFactory
 *
 * @author carlosdaniel
 */
class TrainerFactory {
    
    public static function getTrainer($nombre, $medallas, $edad, $pueblo, $pokemons): \Trainer {
        return new Trainer($nombre, $medallas, $edad, $pueblo, $pokemons);
    }
    public static function getTrainerAmistoso($nombre, $medallas, $edad, $pueblo,$pokemons): \TrainerAmistoso {
        return new TrainerAmistoso($nombre, $medallas, $edad, $pueblo,$pokemons);
    }
    public static function getTrainerAventajado($nombre, $medallas, $edad, $pueblo,$pokemons): \TrainerAventajado {
        return new TrainerAventajado($nombre, $medallas, $edad, $pueblo,$pokemons);
    }
    public static function getTrainerCompetitivo($nombre, $medallas, $edad, $pueblo,$pokemons): \TrainerCompetitivo {
        return new TrainerCompetitivo($nombre, $medallas, $edad, $pueblo,$pokemons);
    }
}
