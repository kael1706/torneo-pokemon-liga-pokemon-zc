<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BatallaFactory
 *
 * @author carlosdaniel
 */
class BatallaFactory {
    //put your code here
    public static function getBatalla($trainer1, $trainer2): \Batalla {
        return new Batalla($trainer1, $trainer2);
    }
}
