<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AtaqueFactory
 *
 * @author carlosdaniel
 */
class AtaqueFactory implements IAtaqueFactory{
    
    public static function getAtaque($nombre, $tipo, $danio, $pp): \Ataque {
        return new Ataque($nombre, $tipo, $danio, $pp);
    }
    
}
