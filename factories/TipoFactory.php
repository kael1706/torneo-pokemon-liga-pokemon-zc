<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoFactory
 *
 * @author carlosdaniel
 */
class TipoFactory implements ITipoFactory {
    
    public static function getTipo($nombre, $fortaleza, $debilidades): \Tipo {
        return new Tipo($nombre, $fortaleza, $debilidades);
    }
    
}
